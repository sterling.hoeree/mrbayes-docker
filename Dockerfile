FROM jenkins:2.19.3

USER root
RUN apt-get update \
      && apt-get install -y sudo \
      && rm -rf /var/lib/apt/lists/*
RUN echo "jenkins ALL=NOPASSWD: ALL" >> /etc/sudoers

RUN apt-get update && \
	apt-get install -y wget tar build-essential make autoconf automake \
        libtool subversion pkg-config vim gettext-base less

ENV PATH "$PATH:/var/jenkins_home/bin"
ENV LD_LIBRARY_PATH "$LD_LIBRARY_PATH:/var/jenkins_home/lib"

USER jenkins
COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt

COPY build.sh .
#RUN ./build.sh  # Must be done AFTER mounting shared volume /var/jenkins_home

