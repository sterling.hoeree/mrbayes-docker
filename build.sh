#!/usr/bin/env bash
set -xe

# Generate SSH key
if [ ! -e $HOME/.ssh/id_rsa ]; then
    mkdir -p $HOME/.ssh/
    echo -e "\n\n\n" | ssh-keygen -t rsa -N ""
    touch $HOME/.ssh/known_hosts
    ssh-keyscan -H github.com >> $HOME/.ssh/known_hosts 
    chmod 600 $HOME/.ssh/known_hosts
fi

# TODO: install OpenCL (see https://wiki.tiker.net/OpenCLHowTo)


# Download and install MPI
sudo apt-get install -y libopenmpi-dev openmpi-bin openmpi-doc

# Download BEAGLE
cd $HOME
if [ ! -d beagle-lib ]; then
    git clone --depth=1 https://github.com/beagle-dev/beagle-lib.git
fi
if [ ! -e $HOME/lib/libhmsbeagle.so ]; then
    cd beagle-lib
    sudo ./autogen.sh
    sudo ./configure --prefix=$HOME
    sudo make
    sudo make install
fi


# Download and install MrBayes with BEAGLE and MPI enabled
cd $HOME
if [ ! -d MrBayes-3.2.6 ]; then
    wget https://github.com/NBISweden/MrBayes/archive/v3.2.6.tar.gz
    tar -xzvf v3.2.6.tar.gz && rm v3.2.6.tar.gz
fi
cd MrBayes-3.2.6
if [ ! -d src ]; then
    tar -xzvf mrbayes-3.2.6.tar.gz
    rm *.zip *.tar.gz
    mv mrbayes-3.2.6/* ./
    rmdir mrbayes-3.2.6
fi

cd src
sudo autoconf
sudo ./configure --prefix=$HOME --with-beagle=$HOME --enable-mpi
sudo make
sudo make install

